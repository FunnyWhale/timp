//
// Created by mv on 13.04.2019.
//
#include "task2-5.h"
#include <iterator>
#include <algorithm>
#include <iostream>

using namespace std;

CGraph::CGraph(bool **matrix, int *arr, int quant) {
    this->quant = quant;
    for (int i = 0; i < this->quant; ++i) {
        nodeArr.push_back(new Node(arr[i]));
    }
    adjMatrix = matrix;
    nodeArr[0]->prev = nullptr;
}

bool CGraph::BFsearch(int value) {
    path.clear();
    if (nodeArr.size()) {
        greyQueue.push(nodeArr[0]);
        while (greyQueue.size()) {
            Node *node = greyQueue.front();
            greyQueue.pop();
            if (node->val == value) {
                while (!greyQueue.empty())
                    greyQueue.pop();
                for (auto i: nodeArr)
                    i->nodeColour = Node::WHITE;
                do {
                    path.push_back(to_string(node->val));
                    path.push_back(",");
                    //cout << node->val << ",";
                    node = node->prev;
                } while (node != nullptr);
                reverse(path.begin(), path.end());
                path.erase(path.begin());
                for (auto i: path)
                    cout << i;
                cout << endl;
                return true;
            }

            vector<Node *>::iterator it = find(nodeArr.begin(), nodeArr.end(), node);
            int index = distance(nodeArr.begin(), it);
            for (int i = 0; i < quant; ++i) {
                if (adjMatrix[index][i])
                    if (nodeArr[i]->nodeColour == Node::WHITE) {
                        nodeArr[i]->prev = node;
                        nodeArr[i]->nodeColour = Node::GREY;
                        greyQueue.push(nodeArr[i]);
                    }
            }
            node->nodeColour = Node::BLACK;
        }

    }
    return false;
}

string CGraph::DFS(int value) {
    int top = 0;
    for (int j = 0; j < quant; j++) {
        if (value == nodeArr[j]->val) {
            top = j;
            break;
        }
    }
    nodeArr[top]->nodeColour = Node::BLACK;
    components += to_string(nodeArr[top]->val);
    for (int i = 0; i < quant; i++) {
        if (adjMatrix[top][i] && (nodeArr[i]->nodeColour != Node::BLACK))
            DFS(nodeArr[i]->val);
    }


    return components;
}




string CGraph::Components() {
    for (auto i : nodeArr)
        i->nodeColour = Node::WHITE;
    for (int i = 0; i < quant; i++) {
        if (nodeArr[i]->nodeColour != Node::BLACK) {
            components.clear();
            DFS(nodeArr[i]->val);
            comp += components + ",";
        }
    }
    return comp;
}
