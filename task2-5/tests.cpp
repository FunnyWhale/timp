//
// Created by mv on 14.04.2019.
//

#define CATCH_CONFIG_MAIN

#include "../catch.hpp"
#include "task2-5.h"

TEST_CASE("1" "2") {
#define quant 7
    bool **arr = new bool *[quant];
    for (int i = 0; i < quant; ++i) {
        arr[i] = new bool[quant];
    }
    arr[0][0] = 0;
    arr[0][1] = 1;
    arr[0][2] = 0;
    arr[0][3] = 0;
    arr[0][4] = 1;
    arr[0][5] = 0;
    arr[0][6] = 0;

    arr[1][0] = 1;
    arr[1][1] = 0;
    arr[1][2] = 1;
    arr[1][3] = 1;
    arr[1][4] = 0;
    arr[1][5] = 0;
    arr[1][6] = 0;

    arr[2][0] = 0;
    arr[2][1] = 1;
    arr[2][2] = 0;
    arr[2][3] = 0;
    arr[2][4] = 0;
    arr[2][5] = 0;
    arr[2][6] = 0;

    arr[3][0] = 0;
    arr[3][1] = 1;
    arr[3][2] = 0;
    arr[3][3] = 0;
    arr[3][4] = 0;
    arr[3][5] = 0;
    arr[3][6] = 0;

    arr[4][0] = 1;
    arr[4][1] = 0;
    arr[4][2] = 0;
    arr[4][3] = 0;
    arr[4][4] = 0;
    arr[4][5] = 0;
    arr[4][6] = 0;

    arr[5][0] = 0;
    arr[5][1] = 0;
    arr[5][2] = 0;
    arr[5][3] = 0;
    arr[5][4] = 0;
    arr[5][5] = 0;
    arr[5][6] = 1;

    arr[6][0] = 0;
    arr[6][1] = 0;
    arr[6][2] = 0;
    arr[6][3] = 0;
    arr[6][4] = 0;
    arr[6][5] = 1;
    arr[6][5] = 0;

    int vals[quant] = {1, 2, 3, 4, 5, 6, 7};
    CGraph graph(arr, vals, quant);
    REQUIRE(graph.BFsearch(6) == false);
    REQUIRE(graph.BFsearch(1) == true);
    REQUIRE(graph.BFsearch(1) == true);
    REQUIRE(graph.BFsearch(2) == true);
    REQUIRE(graph.Components() == "12345,67,");
}