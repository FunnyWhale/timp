//
// Created by mv on 13.04.2019.
//
#ifndef TIMP_TASK2_5_H
#define TIMP_TASK2_5_H

#include <queue>
#include <string>
#include <vector>

using namespace std;

class CGraph {
private:

    class Node {
    private:
        enum colour {
            WHITE, GREY, BLACK
        };
    public:
        Node *prev;
        int val;
        colour nodeColour;

        Node() {
            nodeColour = WHITE;
            val = 0;
        };

        Node(int value) {
            nodeColour = WHITE;
            val = value;
        };

        friend class CGraph;
    };

    bool **adjMatrix;
    vector<Node *> nodeArr;
    queue<Node *> greyQueue;
    int quant;
public:
    CGraph(bool **matrix, int *arr, int quant);
    string DFS(int value);
    bool BFsearch(int value);


    vector<string> path;

    string Components();
    string comp,components;
};

#endif //TIMP_TASK2_5_H