//
// Created by mv on 06.04.2019.
//
#define CATCH_CONFIG_MAIN

#include "../catch.hpp"
#include "task2-3.h"
#include <algorithm>

TEST_CASE("1", "task2-3") {
    CTrie trie;
    REQUIRE(trie.search("")== false);
    trie.insert("");
    REQUIRE(trie.search("")==true);
    trie.insert("aa");
    REQUIRE(trie.search("a")==false);
    REQUIRE(trie.search("aa")==true);
    trie.insert("aa");
    REQUIRE(trie.search("aa")==true);
    trie.insert("a");
    REQUIRE(trie.search("a")==true);

}

