//
// Created by mv on 06.04.2019.
//

#include "task2-3.h"
#include <iterator>
#include <algorithm>
#include <iostream>

using namespace std;

void CTrie::show() {
    print();
}

void CTrie::print(string prev_str) {

    if (val.size())
        prev_str += val;
    if (isLeaf)
        std::cout <<"\""<< prev_str <<"\""<< endl;
    for (CTrie *node : subTrie)
        node->print(prev_str);

}

void CTrie::insert(string key) {

    if(!key.size())
        this->isLeaf=true;
    CTrie *ptr = this;
    for (string::iterator i = key.begin(); i != key.end(); i++) {


        string letter;
        letter = *i;
//
        if (!(any_of(std::begin(ptr->subTrie), std::end(ptr->subTrie), [letter](CTrie *p) {
            return p->val == letter;
        }))) {
            CTrie *trie = new CTrie;
            trie->val = letter;
            ptr->subTrie.push_back(trie);
            ptr = *(find_if(begin(ptr->subTrie), end(ptr->subTrie),
                            [trie](CTrie *p) {
                                return p == trie;
                            }));
            if (i + 1 == key.end()) {
                ptr->isLeaf = true;

            }
        } else {
            vector<CTrie *>::iterator it = find_if(begin(ptr->subTrie), end(ptr->subTrie),
                                                   [letter](CTrie *p) {

                                                       return p->val == letter;
                                                   });

            if (it != ptr->subTrie.end())
                ptr = *it;

            if (i + 1 == key.end()) {
                ptr->isLeaf = true;
            }

        }
    }

}

bool CTrie::search(string key) {
    if(!key.size())
        return this->isLeaf;
    CTrie * ptr = this;
    for(string::iterator i = key.begin();i!=key.end();i++) {

        string letter;
        letter = *i;
        vector<CTrie *>::iterator it = find_if(begin(ptr->subTrie), end(ptr->subTrie),
                                               [letter](CTrie *p) {

                                                   return p->val == letter;
                                               });
        if (it != ptr->subTrie.end())
            ptr = *it;
        else
            return false;
        if (i + 1 == key.end()){
                return ptr->isLeaf;
            }
    }

}