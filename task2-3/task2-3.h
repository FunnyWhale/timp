//
// Created by mv on 06.04.2019.
//

#ifndef TIMP_CTRIE_H
#define TIMP_CTRIE_H

#include <string>
#include <vector>

using namespace std;

class CTrie {
//private:
public:
    void show();
    void print(string prev_str = "");
    bool isLeaf;
    vector<CTrie *> subTrie;
    string val;

    CTrie() {
        isLeaf = false;
        val = "";
    }



//    ~CTrie();


    void insert(string key);

    bool search(string key);
};


#endif //TIMP_CTRIE_H
