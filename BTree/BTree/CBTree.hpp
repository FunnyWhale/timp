#pragma once
class CBTree
{
private:
	class Tip
	{
	public:
		static const int t = 2;
		int keys[2 * t];//������ ������	
		bool isleaf;//�������� �� ����
		Tip* child[2 * t + 1];//������ �����������
		Tip* parent;//��������
		int quant;//���������� ������	
		int quantSons;//���������� �����������
		int height;
		friend CBTree;
		Tip();
	};

	void insertToTip(int key, Tip* tip);//������� � ����
	void sort(Tip* tip);//���������� ����
	void restruct(Tip* tip);//��������� ����
	void delTip(Tip*);//�������� ����
	bool searchKey(int key, Tip* tip);//����� �� �����
	void remove(int key, Tip * tip);//��������
	void removeFromTip(int key, Tip* tip);//�������� �� ����
	void removeLeaf(int key, Tip* tip);//�������� �� �����
	void lconnect(Tip* node, Tip* tip);//����������� �����
	void rconnect(Tip* node, Tip* tip);//
	void repair(Tip* tip);//����������� ���� ������� ������
	int treeHeight;
public:
    Tip* root;
	CBTree();
	~CBTree();
	void insert(int key);
	bool search(int key);
	void remove(int key);
	int getHeight(Tip* tip);
};
