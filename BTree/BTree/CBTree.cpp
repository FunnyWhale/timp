#include "pch.h"
#include "CBTree.hpp"

CBTree::CBTree() {
    root = nullptr; //������ ����� ��������� �������
    treeHeight = 0;
}

CBTree::Tip::Tip() {
    height = 1;
    isleaf = 1;
    quant = 0;
    quantSons = 0;
}

CBTree::~CBTree() {
    if (root != nullptr)//���� ������ �� ������, �������
        delTip(root);
}

void CBTree::delTip(Tip *tip) {
    if (tip != nullptr)//���� ���� - �� ����, ���������� �� ������� �� ����������� � �������
    {
        for (int i = 0; i <= 2 * Tip::t - 1; i++) {
            if (tip->child[i] != nullptr)
                delTip(tip->child[i]);
            else {
                delete (tip);
                break;
            }
        }
    }
}

void CBTree::insertToTip(int key, Tip *tip) {//������� ����������
    tip->keys[tip->quant] = key;//��������� ����
    tip->quant++;//����������� ���������� ������
    sort(tip);//��������� �����
}

void CBTree::sort(Tip *tip) {//������� �������, ����������� ����� ��� ������� ����
    int buf;
    for (int i = 0; i < 2 * Tip::t - 1; i++) {
        for (int j = i + 1; j <= 2 * Tip::t + 1; i++) {
            if (tip->keys[i] != 0 && tip->keys[j] != 0) {
                if (tip->keys[i] > tip->keys[j]) {
                    buf = tip->keys[i];
                    tip->keys[i] = tip->keys[j];
                    tip->keys[j] = buf;
                }
            } else
                break;

        }
    }

}

void CBTree::insert(int key) {//���������� � ��������������� ������� ������ �������
    if (root == nullptr)//���� ������ ������, ������ ������ ���� � ������, ���������� � �����
    {
        Tip *newTip = new Tip;//������� ��������� ����
        newTip->keys[0] = key;//������ ���� - ���������� � �������
        for (int j = 1; j <= 2 * Tip::t - 1; j++)// ��������� ��������� ����� ������
            newTip->keys[j] = 0;//
        for (int i = 0; i <= 2 * Tip::t; i++)//
            newTip->child[i] = nullptr;//������ ��� ��������� ����������� �� �������
        newTip->quant = 1;//���������� ������ = 1
        newTip->quantSons = 0;// ���������� ����������� = 0
        newTip->isleaf = true;// ���������� ���� ��� ����
        newTip->parent = nullptr;//����������, ��� �� �� ���� �� �����������
        root = newTip;// ������ ������ ��������� ����
    } else//���� ������ �� ������
    {
        Tip *ptr = root;//������� ���������, ������ ��� �� ������
        while (ptr->isleaf == false)//���� ��������� �� ��������� �� ����
        {
            for (int i = 0; i <= 2 * Tip::t - 1; i++)//���������� �����
            {
                if (ptr->keys[i] != 0)//���� i-� ���� - �� 0, �.�. ������ ����
                {
                    if (key < ptr->keys[i])//���� ������ ���� ������ i-��
                    {
                        ptr = ptr->child[i];//������ ��������� �� i-�� ����������
                        break;//������� �� for
                    }
                    if (ptr->keys[i + 1] == 0 &&
                        key > ptr->keys[i])//���� i+1-� ���� = 0 � ���������� ���� ������ i-�� �����
                    {
                        ptr = ptr->child[i + 1];//������ ��������� �� i+1-�� ����������
                        break;//������� �� for
                    }
                } else
                    break;//������� �� for
            }
        }
        insertToTip(key, ptr);//��������� ������� � ���������� ����
        while (ptr->quant == 2 * Tip::t) {
            if (ptr == root)//
            {
                restruct(ptr);//��������� ����
                break;
            } else//
            {
                restruct(ptr);//��������� ����
                ptr = ptr->parent;//
            }
        }
    }
}

void CBTree::restruct(Tip *tip) {//��������� ���� �� 2
    if (tip->quant < (2 * Tip::t - 1))//���� ���������� ������ ���������� ����
        return;//�� ���������
    Tip *child1 = new Tip;//������ ���������
    for (int j = 0; j <= Tip::t - 2; j++)//����������� �������� ������ ����������
        child1->keys[j] = tip->keys[j];
    for (int j = Tip::t - 1; j <= (2 * Tip::t - 1); j++)//��������� ����� - ����(������)
        child1->keys[j] = 0;//
    child1->quant = Tip::t - 1;//������������� ���������� ������
    if (tip->quantSons != 0)//���� ����������� �� ����
    {
        for (int i = 0; i <= Tip::t - 1; i++) {
            child1->child[i] = tip->child[i];//��������� ����������� � ����
            child1->child[i]->parent = child1;//��� ���� ����������� ������ ��������� ����
        }
        for (int i = Tip::t; i <= 2 * Tip::t; i++)//
            child1->child[i] = nullptr;// ������� ���� �����������
        child1->isleaf = false;// �� ����
        child1->quantSons = Tip::t - 1;// ������ ���������� �����������
    } else//����� ���� ����������� ����
    {
        child1->isleaf = true;//����
        child1->quantSons = 0;//����������� ����
        for (int i = 0; i <= Tip::t * 2; i++)
            child1->child[i] = nullptr;//������� �����������

    }
    Tip *child2 = new Tip;//������ ���������
    for (int i = 0; i <= Tip::t - 1; i++)//����������� �����, ������� � t
        child2->keys[i] = tip->keys[i + Tip::t];//
    for (int i = Tip::t; i <= 2 * Tip::t - 1; i++)//
        child2->keys[i] = 0;//��������� ����� - ����(�����)
    child2->quant = Tip::t;//������ ���������� ������ �� t
    if (tip->quantSons != 0)//���� ���� ����������
    {//
        for (int i = 0; i <= Tip::t; i++)//
        {//
            child2->child[i] = tip->child[i + Tip::t];//��������� �����������
            child2->child[i]->parent = child2;//��� ����������� ������ ��������
        }
        for (int i = Tip::t + 1; i < 2 * Tip::t; i++)//
            child2->child[i] = nullptr;//������� �����������
        child2->isleaf = false;//�� ����
        child2->quantSons = Tip::t;//���������� ����������� - t
    } else {
        child2->isleaf = true;//����
        child2->quantSons = 0;//���������� ����������� - 0
        for (int i = 0; i <= 2 * Tip::t; i++)//
            child2->child[i] = nullptr;//������� �����������
    }

    if (tip->parent == nullptr)//���� tip - ������
    {
        tip->keys[0] = tip->keys[Tip::t - 1];//������ ������ ������ ����������
        for (int i = 1; i < 2 * Tip::t - 1; i++)//��� ��������� ����� - 0
            tip->keys[i] = 0;//
        tip->child[0] = child1;//
        tip->child[1] = child2;//
        for (int i = 2; i <= Tip::t * 2; i++)//��� ����������, ����� child1 & child2 - ������
            tip->child[i] = nullptr;//
        tip->parent = nullptr;//
        tip->isleaf = false;//
        tip->quant = 1;//
        tip->quantSons = 2;//
        child1->parent = tip;//
        child2->parent = tip;//
        child1->height = child1->parent->height + 1;
        child2->height = child2->parent->height + 1;
    }///
    else//
    {
        insertToTip(tip->keys[Tip::t - 1], tip->parent);//��������� ���������� ���� � ��������
        for (int i = 0; i <= Tip::t * 2; i++) {
            if (tip->parent->child[i] == tip)//
                tip->parent->child[i] = nullptr;//
        }//
        for (int i = 0; i <= 2 * Tip::t; i++)//
        {
            if (tip->parent->child[i] == nullptr)//
            {
                for (int j = 2 * Tip::t; j > i + 1; j--)//
                    tip->parent->child[j] = tip->parent->child[j - 1];//
                tip->parent->child[i + 1] = child2;//
                tip->parent->child[i] = child1;//
                break;//
            }
        }
        child1->parent = tip->parent;//
        child2->parent = tip->parent;//
        tip->parent->isleaf = false;//
        child1->height = child1->parent->height + 1;
        child2->height = child2->parent->height + 1;//
    }

}

bool CBTree::search(int key) {
    return searchKey(key, this->root);
}

bool CBTree::searchKey(int key, Tip *tip) {
    if (tip != nullptr) {
        if (tip->isleaf == false) {//���� ���� - �� ����, � ����� ���� ����
            int i;
            for (i = 0; i <= (2 * Tip::t - 1); i++) {
                if (tip->keys[i] != 0) {
                    if (key == tip->keys[i]) return true;
                    if ((key < tip->keys[i])) {
                        return searchKey(key, tip->child[i]);
                        break;
                    }
                } else break;
            }
            return searchKey(key, tip->child[i]);//���� � ������ ����������, ���� �����
        } else {//����� ���� ������ � ������ ����
            for (int j = 0; j <= (2 * Tip::t - 1); j++)
                if (key == tip->keys[j]) return true;
            return false;
        }
    } else return false;
}

void CBTree::removeFromTip(int key, Tip *tip) {
    for (int i = 0; i < tip->quant; i++) {//� ����� ���� ���� � ���� �, ���� �������, �������
        if (tip->keys[i] == key) {
            for (int j = i; j < tip->quant; j++) {//
                tip->keys[j] = tip->keys[j + 1];
                tip->child[j] = tip->child[j + 1];
            }
            tip->keys[tip->quant - 1] = 0;
            tip->child[tip->quant - 1] = tip->child[tip->quant];
            tip->child[tip->quant] = nullptr;
            break;
        }
    }
    tip->quant--;
}

void CBTree::lconnect(Tip *tip, Tip *othertip) {
    if (tip == nullptr) return;//���� ������ ���� ������, ������ ������������
    for (int i = 0; i <= (othertip->quant - 1); i++) {//
        tip->keys[tip->quant] = othertip->keys[i];//��������� ����
        tip->child[tip->quant] = othertip->child[i];//��������� ����������
        tip->quant = tip->quant + 1;//����������� ���������� ������
    }
    tip->child[tip->quant] = othertip->child[othertip->quant];
    for (int j = 0; j <= tip->quant; j++) {
        if (tip->child[j] == nullptr) break;
        tip->child[j]->parent = tip;
    }
    delete othertip;// ������� ������ ����
}

void CBTree::rconnect(Tip *tip, Tip *othertip) {
    if (tip == nullptr) return;
    for (int i = 0; i <= (othertip->quant - 1); i++) {
        tip->keys[tip->quant] = othertip->keys[i];
        tip->child[tip->quant + 1] = othertip->child[i + 1];
        tip->quant = tip->quant + 1;
    }
    for (int j = 0; j <= tip->quant; j++) {
        if (tip->child[j] == nullptr) break;
        tip->child[j]->parent = tip;
    }
    delete othertip;
}

void CBTree::repair(Tip *tip) {
    if ((tip == root) && (tip->quant == 0)) {
        if (root->child[0] != nullptr) {//
            root->child[0]->parent = nullptr;//
            root = root->child[0];//
        }//
        else {//
            delete root;//
        }//
        return;//
    }
    Tip *ptr = tip;//
    int positionSon;//
    Tip *parent = ptr->parent;//
    for (int j = 0; j <= parent->quant; j++) {//
        if (parent->child[j] == ptr) {//
            positionSon = j; //������� ���� �� ��������� � ��������
            break;
        }
    }
    //���� ptr-������ ������� (����� �����)
    if (positionSon == 0) {//
        insertToTip(parent->keys[positionSon], ptr);//
        lconnect(ptr, parent->child[positionSon + 1]);//
        parent->child[positionSon + 1] = ptr;//
        parent->child[positionSon] = nullptr;//
        removeFromTip(parent->keys[positionSon], parent);//
        if (ptr->quant == 2 * Tip::t) {//
            while (ptr->quant == 2 * Tip::t) {//
                if (ptr == root) {//
                    restruct(ptr);//
                    break;//
                } else {//
                    restruct(ptr);//
                    ptr = ptr->parent;//
                }
            }
        } else if (parent->quant <= (Tip::t - 2))
            repair(parent);//
    } else {
        //���� ptr-��������� ������� (����� ������)
        if (positionSon == parent->quant) {//
            insertToTip(parent->keys[positionSon - 1], parent->child[positionSon - 1]);//
            lconnect(parent->child[positionSon - 1], ptr);//
            parent->child[positionSon] = parent->child[positionSon - 1];//
            parent->child[positionSon - 1] = nullptr;//
            removeFromTip(parent->keys[positionSon - 1], parent);//
            Tip *temp = parent->child[positionSon];//
            if (ptr->quant == 2 * Tip::t) {//
                while (temp->quant == 2 * Tip::t) {//
                    if (temp == root) {//
                        restruct(temp);//
                        break;
                    } else {
                        restruct(temp);//
                        temp = temp->parent;//
                    }
                }
            } else if (parent->quant <= (Tip::t - 2)) repair(parent);//
        } else { //���� ptr ����� ������� ������ � �����
            insertToTip(parent->keys[positionSon], ptr);//
            lconnect(ptr, parent->child[positionSon + 1]);//
            parent->child[positionSon + 1] = ptr;//
            parent->child[positionSon] = nullptr;//
            removeFromTip(parent->keys[positionSon], parent);//
            if (ptr->quant == 2 * Tip::t) {//
                while (ptr->quant == 2 * Tip::t) {//
                    if (ptr == root) {//
                        restruct(ptr);//
                        break;//
                    }//
                    else {//
                        restruct(ptr);//
                        ptr = ptr->parent;//
                    }//
                }//
            }//
            else//
            if (parent->quant <= (Tip::t - 2)) repair(parent);//
        }
    }
}

void CBTree::removeLeaf(int key, Tip *tip) {//�������� ����� �� �����
    if ((tip == root) && (tip->quant == 1)) {//
        removeFromTip(key, tip);//
        root->child[0] = nullptr;//
        delete root;//
        root = nullptr;//
        return;//
    }
    if (tip == root) {//
        removeFromTip(key, tip);//
        return;//
    }
    if (tip->quant > (Tip::t - 1)) {//
        removeFromTip(key, tip);//
        return;//
    }
    Tip *ptr = tip;//
    int k1;//
    int k2;//
    int position;//
    int positionSon;//
    for (int i = 0; i <= tip->quant - 1; i++) {//
        if (key == tip->keys[i]) {//
            position = i; //������� ����� � ����
            break;
        }
    }
    Tip *parent = ptr->parent;//
    for (int j = 0; j <= parent->quant; j++) {//
        if (parent->child[j] == ptr) {//
            positionSon = j; //������� ���� �� ��������� � ��������
            break;
        }
    }
    //���� ptr-������ ������� (����� �����)
    if (positionSon == 0) {//
        if (parent->child[positionSon + 1]->quant > (Tip::t - 1)) { //���� � ������� ����� ������, ��� t-1 ������
            k1 = parent->child[positionSon + 1]->keys[0]; //k1 - ����������� ���� ������� �����
            k2 = parent->keys[positionSon]; //k2 - ���� ��������, ������, ��� ���������, � ������, ��� k1
            insertToTip(k2, ptr);//
            removeFromTip(key, ptr);//
            parent->keys[positionSon] = k1; //������ ������� k1 � k2
            removeFromTip(k1, parent->child[positionSon + 1]); //������� k1 �� ������� �����
        } else { //���� � ������� <u>�������������</u> ����� �� ������ t-1 ������
            removeFromTip(key, ptr);//
            if (ptr->quant <= (Tip::t - 2)) repair(ptr);//
        }
    } else {
        //���� ptr-��������� ������� (����� ������)
        if (positionSon == parent->quant) {
            //���� � ������ ����� ������, ��� t-1 ������
            if (parent->child[positionSon - 1]->quant > (Tip::t - 1)) {
                Tip *temp = parent->child[positionSon - 1];
                k1 = temp->keys[temp->quant - 1]; //k1 - ������������ ���� ������ �����
                k2 = parent->keys[positionSon - 1]; //k2 - ���� ��������, ������, ��� ��������� � ������, ��� k1
                insertToTip(k2, ptr);//
                removeFromTip(key, ptr);//
                parent->keys[positionSon - 1] = k1;//
                removeFromTip(k1, temp);//
            } else { //���� � <u>�������������</u> ������ ����� �� ������ t-1 ������
                removeFromTip(key, ptr);//
                if (ptr->quant <= (Tip::t - 2)) repair(ptr);//
            }
        } else { //���� ptr ����� ������� ������ � �����
            //���� � ������� ����� ������, ��� t-1 ������
            if (parent->child[positionSon + 1]->quant > (Tip::t - 1)) {
                k1 = parent->child[positionSon + 1]->keys[0]; //k1 - ����������� ���� ������� �����
                k2 = parent->keys[positionSon]; //k2 - ���� ��������, ������, ��� ��������� � ������, ��� k1
                insertToTip(k2, ptr);//
                removeFromTip(key, ptr);//
                parent->keys[positionSon] = k1; //������ ������� k1 � k2
                removeFromTip(k1, parent->child[positionSon + 1]); //������� k1 �� ������� �����
            } else {
                //���� � ������ ����� ������, ��� t-1 ������
                if (parent->child[positionSon - 1]->quant > (Tip::t - 1)) {
                    Tip *temp = parent->child[positionSon - 1];//
                    k1 = temp->keys[temp->quant - 1]; //k1 - ������������ ���� ������ �����
                    k2 = parent->keys[positionSon - 1]; //k2 - ���� ��������, ������, ��� ��������� � ������, ��� k1
                    insertToTip(k2, ptr);//
                    removeFromTip(key, ptr);//
                    parent->keys[positionSon - 1] = k1;//
                    removeFromTip(k1, temp);//
                } else { //���� � ����� ������� �� ������ t-1 ������
                    removeFromTip(key, ptr);//
                    if (ptr->quant <= (Tip::t - 2)) repair(ptr);//
                }
            }
        }
    }
}

void CBTree::remove(int key, Tip *tip) {//�������� �� ������������� ����
    Tip *ptr = tip;//
    int position; //����� �����
    for (int i = 0; i <= tip->quant - 1; i++) {//
        if (key == tip->keys[i]) {//
            position = i;//
            break;//
        }
    }
    int positionSon; //����� ���� �� ��������� � ��������
    if (ptr->parent != nullptr) {//
        for (int i = 0; i <= ptr->parent->quant; i++) {//
            if (ptr->child[i] == ptr) {//
                positionSon = i;//
                break;//
            }
        }
    }
    //������� ���������� ���� ������� ���������
    ptr = ptr->child[position + 1];//
    int newkey = ptr->keys[0];//
    while (ptr->isleaf == false) ptr = ptr->child[0];//
    //���� ������ � ��������� ����� �� ������ 1 - ���� ���������� ���� � ����� ���������
    //����� - ������ �������� key �� ����� ����, ������� ����� ���� �� �����
    if (ptr->quant > (Tip::t - 1)) {//
        newkey = ptr->keys[0];//
        removeFromTip(newkey, ptr);//
        tip->keys[position] = newkey;//
    } else {//
        ptr = tip;//
        //���� ���������� ���� � ����� ���������
        ptr = ptr->child[position];//
        newkey = ptr->keys[ptr->quant - 1];//
        while (ptr->isleaf == false) ptr = ptr->child[ptr->quant];//
        newkey = ptr->keys[ptr->quant - 1];//
        tip->keys[position] = newkey;//
        if (ptr->quant > (Tip::t - 1)) removeFromTip(newkey, ptr);//
        else {//
            //���� ������ �� ������, ��� t-1 - �������������
            removeLeaf(newkey, ptr);//
        }
    }
}

void CBTree::remove(int key) {//�������� ����� ��������
    Tip *ptr = this->root;//
    int position;//
    int positionSon;//
    int i;//
    if (searchKey(key, ptr) == false) {//
        return;
    } else {
        //���� ����, � ������� ��������� ���� ��� ��������
        for (i = 0; i <= ptr->quant - 1; i++) {//
            if (ptr->keys[i] != 0) {//
                if (key == ptr->keys[i]) {//
                    position = i;//
                    break;//
                } else {
                    if ((key < ptr->keys[i])) {//
                        ptr = ptr->child[i];//
                        positionSon = i;//
                        i = -1;//
                    } else {//
                        if (i == (ptr->quant - 1)) {//
                            ptr = ptr->child[i + 1];//
                            positionSon = i + 1;//
                            i = -1;//
                        }
                    }
                }
            } else break;
        }
    }
    if (ptr->isleaf == true) {//
        if (ptr->quant > (Tip::t - 1))
            removeFromTip(key, ptr);//
        else removeLeaf(key, ptr);//
    } else remove(key, ptr);//
}

int CBTree::getHeight(Tip *tip) {

    if (tip == nullptr)
        return 0;
    for (auto i: tip->child) {
        if (treeHeight < tip->height)
            treeHeight = tip->height;
        getHeight(i);
    }
    return treeHeight;
}

