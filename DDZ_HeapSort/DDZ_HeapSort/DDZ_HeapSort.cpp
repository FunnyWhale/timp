﻿
#include "pch.h"
#include <iostream>
#include "CHeap.h"
#include "time.h"
using namespace std;
int CHeap::heapsize = 0;
int main()
{
	int arr[] = { 5,3,4,5,1 };
	int quant=50000000;
	int* arr1 = new int[quant];
    for (int j = 0; j < quant; ++j) {
        arr1[j]=rand();
    }
    clock_t start = clock(); // время начала
    CHeap::SortMax(arr1,quant);
    clock_t end = clock(); // время конца
    cout << ((double)end - start) / ((double)CLOCKS_PER_SEC) << endl;//время выполнения алгоритма
}
