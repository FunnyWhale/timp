#pragma once
#include <iterator>
using namespace std;
class CHeap
{
private:
	static int heapsize;
	static int left(int i) { return i * 2 + 1; }
	static int right(int i) { return 2 * i + 2; }
	static void MaxHeapify(int* arr, int i) {
		int l;
		int r;
		int largest;
		while (true) {
			l = left(i);
			r = right(i);
			largest = i;
			if (l < heapsize && arr[l] > arr[largest])
				largest = l;
			if (r < heapsize && arr[r] > arr[largest])
				largest = r;
			if (largest == i) 
				break;
				
			swap(arr[i], arr[largest]);
			i = largest;
		}

	}
	static void MinHeapify(int* arr, int i) {
		int l;
		int r;
		int minimal;
		while (true) {
			l = left(i);
			r = right(i);
			minimal = i;
			if (l < heapsize && arr[l] < arr[i])
				minimal = l;
			if (r < heapsize && arr[r] < arr[minimal])
				minimal = r;
			if (minimal == i) 
				break;
				
			swap(arr[i], arr[minimal]);
			i = minimal;
		}

	}
	static void BuildMaxHeap(int* arr, int arrLength) {
		heapsize = arrLength;
		for (int i = arrLength / 2; i >= 0; i--)
		{
			MaxHeapify(arr, i);
		}
	}
	static void BuildMinHeap(int* arr, int arrLength) {
		heapsize = arrLength;
		for (int i = arrLength / 2; i >= 0; i--)
		{
			MinHeapify(arr, i);
		}
	}

public:
	static void SortMax(int* arr, int arrLength) {
		BuildMaxHeap(arr, arrLength);
		
		for (int i = arrLength-1; i > 0; i--)
		{
			swap(arr[i], arr[0]);
			heapsize--;
			MaxHeapify(arr, 0);
		}
	}
	static void SortMin(int* arr, int arrLength) {
		BuildMinHeap(arr, arrLength);
		for (int i = arrLength-1; i > 0; i--)
		{
			swap(arr[i], arr[0]);
			heapsize--;
			MinHeapify(arr, 0);
		}
	}
};

